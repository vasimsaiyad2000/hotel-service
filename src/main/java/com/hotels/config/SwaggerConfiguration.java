package com.hotels.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
public class SwaggerConfiguration {

	@Bean
	public Docket hotelApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("Hotel API")
				.select().paths(regex("/api/v1/.*"))
				.build();
	}
}
