package com.hotels.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hotels.exceptions.BadRequestException;
import com.hotels.models.Hotels;
import com.hotels.service.HotelService;

@RestController
@RequestMapping(value = "/api/hotels")
public class HotelsController {

	@Autowired
	private HotelService hotelService;

	@PostMapping(value = "")
	public ResponseEntity<Hotels> create(@RequestBody Hotels hotelsVM) {
		Hotels hotels = hotelService.create(hotelsVM);
		return new ResponseEntity<Hotels>(hotels, HttpStatus.CREATED);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<String> update(@PathVariable String id, @RequestBody Hotels hotels) {

		if (hotels == null) {
			throw new BadRequestException("Invalid request body");
		}

		hotels.setId(id);
		hotelService.update(hotels);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Hotels> get(@PathVariable String id) {
		Hotels hotels = hotelService.get(id);
		return new ResponseEntity<Hotels>(hotels, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<String> delete(@PathVariable String id) {
		hotelService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping(value = "")
	public ResponseEntity<Page<Hotels>> search(@RequestParam(required = false) String q) {
		Page<Hotels> results = hotelService.search(q);
		return new ResponseEntity<Page<Hotels>>(results, HttpStatus.OK);
	}
}
