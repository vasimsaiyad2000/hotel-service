package com.hotels.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.hotels.exceptions.ResourceNotFoundException;
import com.hotels.models.Hotels;
import com.hotels.repository.HotelRepository;
import com.hotels.service.HotelService;

@Service
public class HotelServiceImpl implements HotelService {
	
	@Autowired
	private HotelRepository hotelRepository;
	
	@Override
	public Hotels create(Hotels hotels) {
		return hotelRepository.save(hotels);
	}

	@Override
	public void update(Hotels hotels) {
		Hotels entity = get(hotels.getId());
		
		entity.setAddress(hotels.getAddress());
		entity.setAmenties(hotels.getAmenties());
		entity.setCity(hotels.getCity());
		entity.setCountry(hotels.getCountry());
		entity.setDescription(hotels.getDescription());
		entity.setLatitude(hotels.getLatitude());
		entity.setLongitude(hotels.getLongitude());
		
		hotelRepository.save(entity);
	}

	@Override
	public void delete(String id) {
		Hotels hotels = get(id);
		hotelRepository.delete(hotels);
	}

	@Override
	public Hotels get(String id) {
		return hotelRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Hotel not found"));
	}
	

	@Override
	public Page<Hotels> search(String q) {
		
		if (StringUtils.isNotBlank(q)) {
			return hotelRepository.findByNameLikeIgnoreCase(q, PageRequest.of(0, 10));
		}
		
		return hotelRepository.findAll(PageRequest.of(0, 10));
	} 
}
