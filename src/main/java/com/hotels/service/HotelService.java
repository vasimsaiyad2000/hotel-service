package com.hotels.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.hotels.models.Hotels;

public interface HotelService {

	public Hotels create(Hotels hotels);
	public void update(Hotels hotels);
	public void delete(String id);
	public Hotels get(String id);
	public Page<Hotels> search(String q);
	
	
}
