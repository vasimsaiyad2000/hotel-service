package com.hotels.exceptions;

public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8633953433883456075L;

	public BadRequestException() {
        // Do nothing
        super();
    }

    /**
     * Parameterized constructor
     *
     * @param message
     *            exception message
     */
    public BadRequestException(String message) {
        super(message);
    }
}
