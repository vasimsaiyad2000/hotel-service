package com.hotels.exceptions;

public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -860884751477350069L;
	public ResourceNotFoundException() {
        // Do nothing
        super();
    }

    /**
     * Parameterized constructor
     *
     * @param message
     *            exception message
     */
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
