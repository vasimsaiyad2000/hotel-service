package com.hotels.models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "amenties")
public class Amenties {

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
